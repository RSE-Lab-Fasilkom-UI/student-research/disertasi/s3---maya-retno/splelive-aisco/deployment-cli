# ABS Microservices Deployment CLI

Reliable Software Engineering, Faculty of Computer Science, University of Indonesia (2020)

## Table of Contents

Welcome to our code repository hosting ABS Microservices deployment CLI.


1. [General Info](#general-info)
2. [Example Running Product](#example-running-product)
3. [How To Generate Product](#how-to-generate-product)
4. [How to Run the Build Process of ABS Microservices](#how-to-run-the-build-process-of-abs-microservices)
5. [How to Manage Existing Products](#how-to-manage-existing-products)



## General Info

We provide VM for simulating the product generation and run the application. 
Please kindly email hafiyyan@cs.ui.ac.id to request credentials for accessing our VM.
We will send the credentials through email. 

1. Login using the credentials we gave from email.
Make sure that currently you are in home folder of the same username as the credentials we gave.

2. After logged in to the VM, please use `root` user
```bash
sudo su
```
3. Go to folder `deployment-cli` and activate Python virtual environment
```bash
cd deployment-cli
source venv/bin/activate
```

## Example Running Product
We provide a running product representing `SockShop` application. It can be accessed locally from VM through `http://localhost:46339`.
1. You may use CURL inside the VM to access available endpoints from the generated apps. 

2. We also provide the Postman [exported file](webshop-microservices.postman_collection.json) in the Deployment CLI Tool configuration. Currently, we only open port 80 and 443 to the public. You may change the NGINX configuration so that the application will be exposed to the public. The NGINX configuration will be provided inside `/etc/nginx/sites-enabled` folder. Currently, we already have an existing `SockShop` application running in the VM at port 47437. The NGINX configuration for `SockShop` application can be found in `/etc/nginx/sites-enabled/SockShop.conf`.


## How To Generate Product
In this example, we show how to generate a new product. We use HipsterShop product as an example. 
1.  Create a folder inside `deployment-cli/product_config` folder. The folder name must represent the application name. The application name must be **unique**. You can check existing application name inside `deployment-cli/product_config` folder.
```bash
mkdir HipsterShop
```
2.  Create a Basic Configuration file inside the created folder. It will be `deployment-cli/product_config/HipsterShop` if we use the example from Step 1.
```bash
nano basic.json
```

```json
{
  "Cart": [
    "CartUser",
    "CartItem"],
  "Catalog": [
    "DisplayCatalog",
    "CatalogItem"],
  "Order": [
    "ProcessOrder",
    "Shipping"],
  "User": "default"
}
```

3.  Suppose you want to create an advanced configuration file, create a new file using a service name as the filename. 
For example, you want to do advanced configuration for both `Cart`, `Catalog`, and `Order`.
These configuration files also must be under the created folder.
Specifically, we will create it inside `deployment-cli/product_config/HipsterShop` if we use the example from Step 1.

    a. `cart.json` to configure `Cart`.
    ```bash
    nano cart.json
    ```

    ```json
    ["CreateCart",
    "DeleteCart",
    "GetCart",
    "AddItem"]
    ```

    b. `catalog.json` to configure `Catalog`.
    ```bash
    nano catalog.json
    ```

    ```json
    ["DisplayAllItem",
    "FilterByCategory",
    "GetById",
    "SearchItem"
    ]
    ```
    
    c. `order.json` to configure `Order`.
    ```bash
    nano order.json
    ```

    ```json
    ["CreateOrder",
    "ShipOrder"
    ]
    ```
    
4. Invoke the `deplyoment-cli/deployer.py` to generate and deploy the product.
   You may choose **one** application name inside `deployment-cli/product_config` folder.
    ```bash
    python3 deployer.py deploy [ProductName]
    Example:  python3 deployer.py deploy HipsterShop
    ```
    Note: ProductName must be unique.

5. The CLI will show you which port will be used to run the application.
    ```
    Deployment success! You can access the product on: <address-of-the-application>
    ```

6. You may use CURL inside the VM to access available endpoints from the generated apps. 

7. We also provide the Postman exported file as documentation of available API in the application. You can view these exported files [here](postman_list_api) in the Deployment CLI Tool configuration. Currently, we only open port 80 and 443 to the public. You may change the NGINX configuration so that the application will be exposed to the public. The NGINX configuration will be provided inside `/etc/nginx/sites-enabled` folder. Currently, we already have an existing `SockShop` application running in the VM at port 47437. The NGINX configuration for `SockShop` application can be found in `/etc/nginx/sites-enabled/SockShop.conf`.


## How to Manage Existing Products

It is preferrable to perform instructions below using `root` user. Dont forget to activate the Python virtual environment inside `deployment-cli` folder.

### Stop the Product

1.  Invoke the `deployer.py` to stop currently running product.
    ```bash
    python3 deployer.py stop SockShop
    ```
2.  The product will be stopped. When you do a HTTP request to that product's endpoint, it will return `404`.

### Restart the Product

1.  Invoke the `deployer.py` to start or restart a product.
    ```bash
    python3 deployer.py restart SockShop
    ```
2.  The product will be started or restarted. Wait for several seconds before you can access the endpoints.

### Destroy the Product

1.  Invoke the `deployer.py` to destroy an existing product.
    ```bash
    python3 deployer.py destroy SockShop
    ```
2.  The product and databases associated with it will be destroyed. The product configuration you have made earlier in `product_config` folder will not be removed.

