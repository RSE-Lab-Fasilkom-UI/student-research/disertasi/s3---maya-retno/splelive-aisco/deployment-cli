{% if database.type.lower() == "postgresql" %}
CREATE TABLE IF NOT EXISTS products_in_progress (
    id SERIAL,
    name VARCHAR(1000) UNIQUE,
    org_name VARCHAR(1000) UNIQUE,
    subdomain VARCHAR(1000) UNIQUE,
    admin_email VARCHAR(1000) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS generated_products (
    id SERIAL,
    name VARCHAR(1000) UNIQUE,
    org_name VARCHAR(1000) UNIQUE,
    subdomain VARCHAR(1000) UNIQUE,
    admin_email VARCHAR(1000) NOT NULL,
    PRIMARY KEY (id)
);
{% else %}
CREATE TABLE IF NOT EXISTS {{database.root_database}}.products_in_progress (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(1000) UNIQUE,
    `org_name` VARCHAR(1000),
    `subdomain` VARCHAR(1000) UNIQUE,
    `admin_email` VARCHAR(1000) NOT NULL,
    `generator` VARCHAR(1000) NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS {{database.root_database}}.generated_products (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(1000) UNIQUE,
    `org_name` VARCHAR(1000),
    `subdomain` VARCHAR(1000) UNIQUE,
    `admin_email` VARCHAR(1000) NOT NULL,
    `generator` VARCHAR(1000) NOT NULL,
    PRIMARY KEY (`id`)
);
{% endif %}

DELETE FROM {% if database.type.lower() != "postgresql" %}{{database.root_database}}.{% endif %}products_in_progress WHERE name='{{product_name}}';
INSERT INTO {% if database.type.lower() != "postgresql" %}{{database.root_database}}.{% endif %}generated_products (name, org_name, subdomain, admin_email, generator) VALUES ('{{product_name}}', '{{original_product_name}}', '{{product_subdomain}}', '{{user.email}}', '{{generator_codename}}');
