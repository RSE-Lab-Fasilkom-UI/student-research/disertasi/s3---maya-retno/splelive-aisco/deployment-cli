from components.backend import BackendInstance
from components.nginx import NginxInstance


def list_instance_names(payload):
    instance_names = []
    if isinstance(payload["basic_features"], list):
        instance_names = ["nginx"] + payload["basic_features"]
    elif isinstance(payload["basic_features"], dict):
        instance_names = ["nginx"] + list(payload["basic_features"].keys())
    return instance_names


def initiate_instance_objects(payload, config):
    instance_names = list_instance_names(payload)

    instances = {}
    for name in instance_names:
        if name == "nginx":
            instances[name] = NginxInstance(payload, config)
        else:
            instances[name] = BackendInstance(payload,
                                              config,
                                              instance_name=name)
    return instances
